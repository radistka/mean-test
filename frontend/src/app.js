(function() {
  'use strict';

  var app = angular.module('mean-test',
    ['mean-test.core', 'mean-test.movies', 'mean-test.auth']);

  app.config(function($routeProvider) {
    $routeProvider.otherwise({redirectTo:'/login'});
  });
})();
