(function() {
    'use strict';

    angular.module('mean-test.core', [
        /*
         * Angular modules
         */
        'ngRoute',
        /*
         * Our reusable cross app code modules
         */
        'blocks.exception', 'blocks.logger'
    ]).run(function($rootScope, access, logger, $location) {
        $rootScope.$on("$locationChangeStart", function (event, next) {
            if (next.indexOf('movie') != -1 && !access.isAuthorized()) {
                logger.error("You need to be authenticated to see this page!");
                event.preventDefault();
                $location.path('/login')
            }
        });
    });
})();
