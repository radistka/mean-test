'use strict';

(function () {

  angular.module('mean-test.core')
    .factory('rest', rest);

  rest.$inject = ['$http', 'exception', 'access'];

  function rest($http, exception, access) {
    function errorHandler(res) {
      exception.catcher('XHR Failed: ' + res.data.data);
    }

    return function (req) {
      req.headers = {};
      if (access.isAuthorized()) {
        req.headers.token = access.getToken();
      }

      var promise = $http(req);
      promise.then(_.noop, errorHandler);
      return promise;
    };
  }
}());