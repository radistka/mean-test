(function() {
    'use strict';

    angular
        .module('mean-test.core')
        .constant('toastr', toastr)
        .constant('moment', moment);
})();
