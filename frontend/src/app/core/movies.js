(function() {
  angular.module('mean-test.core')
    .factory('movies', movies);

  movies.$inject = ['rest', 'exception'];

  function movies(rest, exception) {
    return {
      getMovies: getMovies,
      updateMovie: updateMovie,
      deleteMovie: deleteMovie,
      createMovie: createMovie
    };

    function getMovies() {
      var req = {
        method: 'GET',
        url: '/api/movies'
      };
      return rest(req);
    }
    function  updateMovie(movieId, data) {
      var req = {
        method: 'PUT',
        url: '/api/movies/' + movieId,
        data: data
      };
      return rest(req);
    }
    function deleteMovie(movieId) {
      var req = {
        method: 'DELETE',
        url: '/api/movies/' + movieId
      };
      return rest(req);
    }
    function createMovie(data) {
      var req = {
        method: 'POST',
        url: '/api/movies',
        data: data
      };
      return rest(req);
    }
  }
})();