(function() {
  "use strict";

  angular.module('mean-test.core')
    .factory('access', access);

  function access() {
    return {
      setToken: setToken,
      getToken: getToken,
      isAuthorized: isAuthorized
    };

    function setToken(token) {
      localStorage.setItem('token', token);
    }

    function getToken() {
      return localStorage.getItem('token');
    }

    function isAuthorized() {
      return !!getToken();
    }
  }
})();