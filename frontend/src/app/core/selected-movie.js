(function() {
  angular.module('mean-test.core')
    .factory('selectedMovie', selectedMovie);

  selectedMovie.$inject = [];

  function selectedMovie() {
    return {
      set: set,
      get: get
    };

    function set(movie) {
      sessionStorage.setItem('movie', JSON.stringify(movie));
    }
    function get() {
      return JSON.parse(sessionStorage.getItem('movie'));
    }
  }
})();