(function() {
  'use strict';

  angular.module('blocks.exception')
    .config(config);


  function config($provide) {
    $provide.decorator('$exceptionHandler', extendExceptionHandler);
  }

  function extendExceptionHandler($delegate, logger) {
    return function(exception, cause) {
      var errorData = {exception: exception, cause: cause};
      $delegate(exception, cause);
      logger.error(exception.message, errorData);
    }
  }
})();