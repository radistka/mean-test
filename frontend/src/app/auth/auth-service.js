(function() {
  angular.module('mean-test.auth')
    .factory('auth', auth);

  auth.$inject = ['rest'];
  function auth(rest) {
    return {
      login: login,
      registration: registration
    };

    function login(data) {
      var  req = {
        method: 'POST',
        url: '/auth/login',
        data: {
          email: data.email,
          password: data.password
        }
      };
      return rest(req);
    }

    function registration(data) {
      var req = {
        method: 'POST',
        url: '/auth/register',
        data: {
          email: data.email,
          password: data.password
        }
      };
      return rest(req);
    }
  }
})();
