(function() {
  "use strict";

  angular.module('mean-test.auth')
    .controller('LoginController', LoginController);

  LoginController.$inject =['auth', 'access', 'toastr', '$location'];

  function LoginController(auth, access, toastr, $location) {
    var vm = this;
    vm.access = access;
    vm.toastr = toastr;
    vm.auth = auth;
    _init();

    function _init() {
      vm.credentials = {};
    }

    vm.login = function() {
      vm.auth.login(vm.credentials).then(function(res) {
        vm.access.setToken(res.data.data);
        toastr.success('Successfully login');
        $location.path('/movies');
      });
    }
  }
})();
