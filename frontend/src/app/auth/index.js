(function() {
  var auth = angular.module('mean-test.auth', []);

  auth.config(function ($routeProvider) {
    $routeProvider
      .when('/login', {
        templateUrl : 'auth/login.html',
        controller  : 'LoginController',
        controllerAs: 'loginController'
      })
      .when('/registration', {
        templateUrl : 'auth/registration.html',
        controller  : 'RegistrationController',
        controllerAs: 'registrationController'
      })
  });
})();
