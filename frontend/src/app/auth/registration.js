(function() {
  "use strict";

  angular.module('mean-test.auth')
    .controller('RegistrationController', RegistrationController);

  RegistrationController.$inject =['auth', 'access', 'toastr', '$location'];

  function RegistrationController(auth, access, toastr, $location) {
    var vm = this;
    vm.access = access;
    vm.toastr = toastr;
    vm.auth = auth;
    _init();

    function _init() {
      vm.credentials = {};
    }

    vm.register = function() {
      vm.auth.registration({
        email: vm.credentials.email,
        password: vm.credentials.password
      }).then(function(res) {
        toastr.success(res.data.data);
        $location.path('/login');
      })
    }
  }
})();
