(function(){
  var movies = angular.module('mean-test.movies', []);

  movies.config(function($routeProvider) {
    $routeProvider
      .when('/movies', {
        templateUrl : 'movies/list.html',
        controller  : 'MoviesListController',
        controllerAs: 'moviesListController',
        resolve: {
          getMovies: getMovies
        }
      })
      .when('/movies/create', {
        templateUrl : 'movies/create.html',
        controller  : 'MovieCreateController',
        controllerAs: 'movieCreateController'
      })
      .when('/movies/update/:id', {
        templateUrl : 'movies/update.html',
        controller  : 'MovieUpdateController',
        controllerAs: 'movieUpdateController'
      });
  });

  function getMovies(movies) {
    return movies.getMovies();
  }
})();