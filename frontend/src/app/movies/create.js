(function() {
  "use strict";

  angular.module('mean-test.movies')
    .controller('MovieCreateController', MovieCreateController);

  MovieCreateController.$inject =['$location', 'movies', 'logger'];

  function MovieCreateController($location, movies, logger) {
    var vm = this;

    _init();

    function _init() {
      console.log('lolo');
      vm.movie = {};
    }

    vm.addMovie = function() {
      movies.createMovie(vm.movie).then(function(res) {
        logger.success('movie successfully created');
        $location.path('/movies');
      }, function(err) {console.log(err)})
    }
  }
})();