(function() {
  "use strict";

  angular.module('mean-test.movies')
    .controller('MoviesListController', MoviesListController);

  MoviesListController.$inject =['getMovies', 'logger', 'movies', 'selectedMovie', '$location'];

  function MoviesListController(getMovies, logger, movies, selectedMovie, $location) {
    var vm = this;

    _init();

    function _init() {
      vm.movies = getMovies.data;
    }

    vm.deleteMovie = function(movie) {
      movies.deleteMovie(movie._id).then(function(res) {
        logger.info(res.data);
        movies.getMovies().then(function(res) {
          vm.movies = res.data;
        })
      })
    };
    vm.updateMovie = function(movie) {
      selectedMovie.set(movie);
      $location.path('/movies/update/' + movie._id);
    }
  }
})();