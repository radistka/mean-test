(function() {
  "use strict";

  angular.module('mean-test.movies')
    .controller('MovieUpdateController', MovieUpdateController);

  MovieUpdateController.$inject =['$location', 'movies', 'logger', 'selectedMovie'];

  function MovieUpdateController($location, movies, logger, selectedMovie) {
    var vm = this;

    _init();

    function _init() {
      vm.movie = selectedMovie.get();
    }

    vm.updateMovie = function() {
      movies.updateMovie(vm.movie._id, vm.movie).then(function(res) {
        logger.success(res.data);
        $location.path('/movies');
      }, function(err) {console.log(err)})
    }
  }
})();