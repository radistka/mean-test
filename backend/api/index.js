module.exports = (function() {
  'use strict';
  const _ = require('lodash');
  const mongoose = require('mongoose');
  const helpers = require('../libs/helpers');
  const apiRouter = require('express').Router();
  const Movie = mongoose.model('Movie');
  const User = mongoose.model('User');
  const movieController = require('./movie-controller');

  var apiController = {};

  apiController.checkToken = function(req, res, next) {
    var token = req.headers.token;

    if (typeof token !== 'undefined') {
      User.findOne({token: token}, (err, user) => {
        if (!err && user) {
          req.body.user = user;
          next();
        } else {
          return helpers.sendResponse(res, 403, {data: 'Permission denied. User with such token does not exist.'});
        }
      });
    } else {
      return helpers.sendResponse(res, 400, {data: 'Bad request, no token finds'});
    }
  };

  var configurations = [
    {
      type: 'get',
      route: '/movies',
      handler: movieController.getMovies,
      middleware: [apiController.checkToken]
    },
    {
      type: 'post',
      route: '/movies',
      handler: movieController.createMovie,
      middleware: [apiController.checkToken]
    },
    {
      type: 'delete',
      route: '/movies/:id',
      handler: movieController.deleteMovieById,
      middleware: [apiController.checkToken]
    },
    {
      type: 'put',
      route: '/movies/:id',
      handler: movieController.updateMovieById,
      middleware: [apiController.checkToken]
    }
  ];

  _.forEach(configurations, (c) => {
    if (c.middleware) {
      apiRouter[c.type](c.route, c.middleware, c.handler);
    } else {
      apiRouter[c.type](c.route, c.handler);
    }
  });

  return apiRouter;
})();