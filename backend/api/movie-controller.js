module.exports = (function() {
  "use strict";
  const mongoose = require('mongoose');
  const helpers = require('../libs/helpers');
  const Movie = mongoose.model('Movie');

  var movieController = {};

  movieController.getMovies = function(req, res) {
    Movie
      .find({})
      .exec((err, movies) => {
        if (err) {
          console.log(err);
          return helpers.sendResponse(res, '500', {data: 'Server error.'});
        } else {
          return helpers.sendResponse(res, '200', movies);
        }
      });
  };

  movieController.getMovieById = function(req, res) {
    Movie
      .findOne({_id: req.params.id})
      .exec((err, movie) => {
        if (err) {
          console.log(err);
          return helpers.sendResponse(res, '500', {data: 'Server error.'});
        } else if (movie) {
          return helpers.sendResponse(res, '200', movie);
        } else {
          return helpers.sendResponse(res, '404', {data: 'Movie with such id does not exist.'});
        }
      });
  };

  movieController.createMovie = function(req, res) {
    var data = req.body;

    var movie = new Movie({
      title: data.title,
      duration: data.duration,
      genre: data.genre,
      rating: data.rating,
      releaseYear: data.releaseYear
    });

    movie.save((err, doc) => {
      if (err) {
        console.log(err);
        return helpers.sendResponse(res, 500, {data: 'Server error.'});
      } else {
        return helpers.sendResponse(res, 200, doc);
      }
    });
  };

  movieController.updateMovieById = function(req, res) {
    var movieId = req.params.id;
    var data = req.body;

    Movie.findOne({_id: movieId}, (err, movie) => {
      if (err) {
        console.log(err);
        return helpers.sendResponse(res, '500', {data: 'Server error.'});
      } else if (movie) {
        Movie.update({_id: movie._id}, { $set: data }, (err, data) => {
          if (err) {
            return helpers.sendResponse(res, '500', {data: 'Server error.'});
          } else {
            return helpers.sendResponse(res, '200', 'Movie successfully updated');
          }
        });
      } else {
        return helpers.sendResponse(res, '404', {data: 'Movie with such id does not exist.'});
      }
    });
  };

  movieController.deleteMovieById = function(req, res) {
    var movieId = req.params.id;

    Movie.remove({_id: movieId}, (err) => {
      if (!err) {
        return helpers.sendResponse(res, '200', 'Movie successfully deleted');
      } else {
        return helpers.sendResponse(res, '500', {data: 'Server error.'});
      }
    });
  };

  return movieController;
})();