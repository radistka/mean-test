(function() {
  'use strict';

  const mongoose = require('mongoose');
  const Schema = mongoose.Schema;

  var UserSchema = new Schema({
    password: {type: String, require: true},
    email: {type: String, unique: true, require: true},
    token: {type: String}
  }, {versionKey: false});

  mongoose.model('User', UserSchema);
})();