'use strict';
(function() {
  const mongoose = require('mongoose');
  const Schema = mongoose.Schema;

  var MovieSchema = new Schema({
    title: {type: String, required: 'Title is required'},
    rating: {type: Number},
    genre: {type: String},
    releaseYear: {type: Number},
    duration: {type: Number}
  }, {versionKey: false});

  mongoose.model('Movie', MovieSchema);
})();

