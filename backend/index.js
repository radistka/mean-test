'use strict';

require('./db');
require('./models');
const path = require('path');
const Server = require('./server');
const server = new Server();
const authRouter = require('./auth');
const apiRouter = require('./api');

server.addNamespace('/auth', authRouter);
server.addNamespace('/api', apiRouter);

server.addNamespace('*', (req, res) => {
  res.status(404).send({message: 'page not found'});
});

server.run();