'use strict';
var helpers = {};

helpers.sendResponse = function (res, code, data) {
  res.writeHead(code, {'Content-Type': 'application/json; charset=utf-8', 'Access-Control-Allow-Origin': '*'});
  res.write(JSON.stringify(data));
  res.end();
};

module.exports = helpers;