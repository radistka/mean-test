module.exports = (function() {
  "use strict";
  const _ = require('lodash');
  const mongoose = require('mongoose');
  const helpers = require('../libs/helpers');
  const crypto = require('crypto');
  const authRouter = require('express').Router();
  const User = mongoose.model('User');
  var authController = {};

  authController.register = function (req, res) {
    var data = req.body;

    User.find({email: data.email}, (err, exist) => {
      if (err) {
        helpers.sendResponse(res, '500', {data: 'Server error.'});
      } else if (exist.length > 0 ) {
        helpers.sendResponse(res, '409', {data: 'User with such email already exist'});
      } else {
        var user = new User({
          password: crypto.createHmac('sha1', data.password).digest('hex'),
          email: data.email
        });

        user.save((err, doc) => {
          if (err) {
            helpers.sendResponse(res, '500', {data: 'Server error.'});
          } else {
            helpers.sendResponse(res, '200', {data: 'User successfully register'});
          }
        });
      }
    });
  };


  authController.login = function (req, res) {
    var email = req.body.email;
    var password = req.body.password;

    User.findOne({email: email}, (err, user) => {
      if (err) {
        helpers.sendResponse(res, '500', {data: 'Server error.'});
      } else if (user) {
        if (user.password === crypto.createHmac('sha1', password).digest('hex')) {
          crypto.randomBytes(36, function(ex, buf) {
            user.token = buf.toString('hex');
            user.save();
            helpers.sendResponse(res, '200', {data:  user.token});

          });
        } else if (user.email && !user.password) {
          helpers.sendResponse(res, '500', {data: 'Please register first.'});
        } else {
          helpers.sendResponse(res, '500', {data: 'Incorrect password.'});
        }
      } else {
        helpers.sendResponse(res, '500', {data: 'User with such email does not exist.'});
      }
    });
  };

  authController.configurations = [
    {type: 'post', route: '/register', handler: authController.register},
    {type: 'post', route: '/login', handler: authController.login}
  ];

  _.forEach(authController.configurations, (c) => {
    if (c.middleware) {
      authRouter[c.type](c.route, c.middleware, c.handler);
    } else {
      authRouter[c.type](c.route, c.handler);
    }
  });
  return authRouter;
})();